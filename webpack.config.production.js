const webpack = require('webpack');
const path = require('path');


// Naming and path settings
const exportPath = path.resolve(__dirname, './webapp/static/dist/');

// Enviroment flag
const env = process.env.WEBPACK_ENV;
const plugins = [];

const config = {
  'config.assets': '"http://localhost:8000/static/"'
};

plugins.push(new webpack.DefinePlugin({
    'process.env': {
      NODE_ENV: '"production"'
    },
    config
  }
));


const MiniCssExtractPlugin = require("mini-css-extract-plugin");

plugins.push(new MiniCssExtractPlugin({
  // Options similar to the same options in webpackOptions.output
  // both options are optional
  filename: "css/[name].css",
  chunkFilename: "css/[id].[hash].css"
}));


const HtmlWebpackPlugin = require('html-webpack-plugin');

plugins.push(new HtmlWebpackPlugin({
  title: 'Incling',
  filename: path.resolve(__dirname, './webapp/templates/webapp/home-production.html'),
  template: 'src/index.html'
}));


// Main Settings config
module.exports = {
  mode: 'production',
  entry: {
    app: './src/app.js',
  },
  output: {
    path: exportPath,
    publicPath: '/static/dist/',
    filename: 'js/[name].min.js',
    chunkFilename: 'js/part.[name].min.js',
    crossOriginLoading: "anonymous"
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /(node_modules|bower_components)/,
        loader: 'babel-loader',
        query: {
          plugins: ['transform-runtime', 'syntax-dynamic-import'],
          presets: ['es2015', 'stage-3']
        },
        include: [path.resolve(__dirname, './src')]
      },
      {
        test: /\.vue$/,
        loader: 'vue-loader',
        options: {
          loaders: [MiniCssExtractPlugin.loader, 'css-loader', 'sass-loader'],
          transformToRequire: {
            video: 'src',
            source: 'src',
            img: 'src',
            image: 'xlink:href'
          }
        }
      },
      {
        test: /\.s?[ac]ss$/,
        use: [MiniCssExtractPlugin.loader, "css-loader", "sass-loader"],
      },
      {
        test: /\.(png|jpe?g|gif|svg)(\?.*)?$/,
        loader: 'url-loader',
        options: {
          limit: 10000,
          name: 'img/[name].[hash:7].[ext]'
        }
      },
      {
        test: /\.(mp4|webm|ogg|mp3|wav|flac|aac)(\?.*)?$/,
        loader: 'url-loader',
        options: {
          limit: 10000,
          name: 'media/[name].[hash:7].[ext]'
        }
      },
      {
        test: /\.(woff2?|eot|ttf|otf)(\?.*)?$/,
        loader: 'url-loader',
        options: {
          limit: 10000,
          name: 'fonts/[name].[hash:7].[ext]'
        }
      }
    ]
  },
  resolve: {
    extensions: ['.js', '.vue', '.json'],
    alias: {
      'vue$': 'vue/dist/vue.esm.js',
      '@': path.resolve(__dirname, './src'),
      'src': path.resolve(__dirname, './src'),
      'assets': path.resolve(__dirname, './src/assets'),
      'components': path.resolve(__dirname, './src/components'),
      '~': path.resolve(__dirname, 'node_modules'),
    }
  },
  plugins
};
