import os
import dj_database_url

from .settings import *


DEBUG = os.environ.get('DEBUG', '1') == '1'

db_from_env = dj_database_url.config()
DATABASES['default'].update(db_from_env)
