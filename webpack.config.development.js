const webpack = require('webpack');
const path = require('path');


// Naming and path settings
const exportPath = path.resolve(__dirname, './webapp/static/');

// Enviroment flag
const env = process.env.WEBPACK_ENV;
const plugins = [];

plugins.push(new webpack.DefinePlugin({
  'config.assets': '"http://localhost:8000/static/"',
  'config.base': '"http://localhost:8000/"',
  'config.api': '"http://localhost:8000/api/1/"'
}));

plugins.push(new webpack.ProvidePlugin({
  $: "jquery",
  jQuery: "jquery",
  "window.jQuery": "jquery",
  Tether: "tether",
  "window.Tether": "tether",
  Alert: "exports-loader?Alert!bootstrap/js/dist/alert",
  Button: "exports-loader?Button!bootstrap/js/dist/button",
  Carousel: "exports-loader?Carousel!bootstrap/js/dist/carousel",
  Collapse: "exports-loader?Collapse!bootstrap/js/dist/collapse",
  Dropdown: "exports-loader?Dropdown!bootstrap/js/dist/dropdown",
  Modal: "exports-loader?Modal!bootstrap/js/dist/modal",
  Popover: "exports-loader?Popover!bootstrap/js/dist/popover",
  Scrollspy: "exports-loader?Scrollspy!bootstrap/js/dist/scrollspy",
  Tab: "exports-loader?Tab!bootstrap/js/dist/tab",
  Tooltip: "exports-loader?Tooltip!bootstrap/js/dist/tooltip",
  Util: "exports-loader?Util!bootstrap/js/dist/util",
  'moment': 'moment',
  'window.moment': 'moment'
}));

// Main Settings config
module.exports = {
  mode: 'development',
  entry: {
    app: './src/app.js',
  },
  output: {
    path: exportPath,
    publicPath: 'http://localhost:8000/static/',
    filename: 'js/[name].js',
    chunkFilename: 'js/part.[name].js',
    crossOriginLoading: "anonymous"
  },
  module: {
    rules: [
      {
        test: /\.html$/,
        use: ['html-loader'],
      },
      { test: /bootstrap[\/\\]dist[\/\\]js[\/\\]umd[\/\\]/, loader: 'imports-loader?jQuery=jquery' },
      {
        test: /\.js$/,
        exclude: /(node_modules|bower_components)/,
        loader: 'babel-loader',
        query: {
          plugins: ['transform-runtime', 'syntax-dynamic-import'],
          presets: ['es2015', 'stage-3']
        }
      },
      {
        test: /\.vue$/,
        loader: 'vue-loader',
        options: {
          loaders: ['style-loader', 'css-loader', 'sass-loader'],
          transformToRequire: {
            video: 'src',
            source: 'src',
            img: 'src',
            image: 'xlink:href'
          }
        }
      },
      {
        test: /\.s?[ac]ss$/,
        use: [
          'style-loader',
          "css-loader",
          "sass-loader"
        ],
      },
      {
        test: /\.(png|jpe?g|gif|svg)(\?.*)?$/,
        loader: 'url-loader',
        options: {
          limit: 10000,
          name: 'img/[name].[hash:7].[ext]'
        }
      },
      {
        test: /\.(mp4|webm|ogg|mp3|wav|flac|aac)(\?.*)?$/,
        loader: 'url-loader',
        options: {
          limit: 10000,
          name: 'media/[name].[hash:7].[ext]'
        }
      },
      {
        test: /\.(woff2?|eot|ttf|otf)(\?.*)?$/,
        loader: 'url-loader',
        options: {
          limit: 10000,
          name: 'fonts/[name].[hash:7].[ext]'
        }
      }
    ]
  },
  resolve:
    {
      extensions: ['.js', '.vue', '.json'],
      alias:
        {
          // 'vue$':
          //   'vue/dist/vue.esm.js',
          // '@':
          //   path.resolve(__dirname, './src'),
          // 'src':
          //   path.resolve(__dirname, './src'),
          // 'assets':
          //   path.resolve(__dirname, './src/assets'),
          // 'components':
          //   path.resolve(__dirname, './src/components'),
          '~':
            path.resolve(__dirname, 'node_modules'),
        }
    }
  ,
  plugins
}
;
