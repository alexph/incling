import angular from 'angular'
import 'angular-resource'
import '@uirouter/angularjs'
import 'angular-formly'
import 'angular-formly-templates-bootstrap'
import 'tempusdominus-bootstrap-4'

import 'bootstrap'

import './scss/app.scss'
import 'tempusdominus-bootstrap-4/build/css/tempusdominus-bootstrap-4.css'

import AppController from './controllers/AppController'
import TileController from './controllers/TileController'
import TaskController from './controllers/TaskController'
import CarouselController from './controllers/CarouselController'
import HomeController from './controllers/HomeController'
import FormController from './controllers/FormController'
import TileResource from './services/TileResource'
import TaskResource from './services/TaskResource'
import TileService from './services/TileService'
import app from './directives/app'
import tile from './directives/tile'
import task from './directives/task'
import carousel from './directives/carousel'
import carouselItem from './directives/carousel-item'
import home from './directives/home'
import mainNav from './directives/main-nav'
import tileForm from './directives/tile-form'
import datetimepicker from './directives/datetimepicker'

const MODULE_NAME = 'app';

const module = angular.module(MODULE_NAME, ['ngResource', 'ui.router', 'formly', 'formlyBootstrap'])
  .controller('AppController', AppController)
  .controller('TileController', TileController)
  .controller('TaskController', TaskController)
  .controller('CarouselController', CarouselController)
  .controller('HomeController', HomeController)
  .controller('FormController', FormController)
  .directive('app', app)
  .directive('tile', tile)
  .directive('task', task)
  .directive('carousel', carousel)
  .directive('carouselItem', carouselItem)
  .directive('home', home)
  .directive('mainNav', mainNav)
  .directive('tileForm', tileForm)
  .directive('datetimepicker', datetimepicker)
  .factory('TileResource', TileResource)
  .factory('TaskResource', TaskResource)
  .factory('TileService', TileService);

module.config(($stateProvider, formlyConfigProvider) => {
  const homeState = {
    name: 'home',
    url: '/',
    template: '<home></home>',
  };

  const addState = {
    name: 'new',
    url: '/new',
    template: '<tile-form></tile-form>',
  };

  const editState = {
    name: 'edit',
    url: '/edit/:id',
    template: '<tile-form></tile-form>'
  };

  $stateProvider.state(homeState);
  $stateProvider.state(addState);
  $stateProvider.state(editState);

  formlyConfigProvider.setWrapper({
    name: 'bootstrapHasError',
    overwriteOk: true,
    template: '<div class="form-group" ng-class="{\'has-error\': showError}">' +
    '<formly-transclude></formly-transclude>' +
    '<ul class="list-unstyled text-danger small" ng-messages="fc.$error" ng-show="options.validation.errorExistsAndShouldBeVisible">' +
    '<li ng-repeat="(name, message) in ::options.validation.messages" ng-message={{::name}}>' +
    '{{ message(fc.$viewValue, fc.$modelValue, this) }}' +
    '</li>' +
    '</ul>' +
    '<div class="text-muted small" ng-if="to.pending && fc.$pending">' +
    '{{::to.pending }}' +
    '</div>' +
    '</div>'
  });

  console.log(formlyConfigProvider);

  formlyConfigProvider.setType({
    name: 'datetimepicker',
    template: '<input type="text" class="form-control datetimepicker-input" ng-model="model[options.key]" datetimepicker />',
    wrapper: ["bootstrapLabel", "bootstrapHasError"]
  })
});

export default module;
