class FormController {
  constructor() {
    this.model = {};

    this.fields = [
      {
        key: 'launch_date',
        type: 'datetimepicker',
        templateOptions: {
          label: 'Launch date',
          placeholder: 'Launch date'
        }
      },
      {
        key: 'status',
        type: 'select',
        templateOptions: {
          label: 'Status',
          options: [
            {
              name: 'Live',
              value: 0
            },
            {
              name: 'Pending',
              value: 1
            },
            {
              name: 'Archived',
              value: 2
            }
          ]
        }
      },
    ];
  }
}

FormController.$inject = [];

export default FormController;
