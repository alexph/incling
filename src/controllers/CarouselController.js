class CarouselController {
  constructor() {
    this.carouselItems = 0;
  }

  registerItem(scope) {
    this.carouselItems++;
  }
}

CarouselController.$inject = [];

export default CarouselController;
