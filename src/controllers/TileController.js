class TileController {
  constructor($scope) {
    this.$scope = $scope;
  }

  deleteTile(tile) {
    if (confirm('Really?')) {
      tile.$delete().then(() => {
        this.$scope.$emit('tileDeleted', tile);
      });
    }
  }
}

TileController.$inject = ['$scope'];

export default TileController;
