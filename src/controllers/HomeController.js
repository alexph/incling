import findIndex from 'lodash/findIndex'

class HomeController {
  constructor($scope, TileService) {
    this.$tileService = TileService;
    this.tiles = [];

    $scope.$on('tileDeleted', (e, tile) => {
      this.deleteTile(tile);
    });

    this.loadTiles();
  }

  loadTiles() {
    this.$tileService.getTiles().then(response => {
      this.tiles = response;
    });
  }

  deleteTile(tile) {
    let idx = findIndex(this.tiles, {id: tile.id});

    if (idx >= 0) {
      this.tiles.splice(idx, 1);
    }
  }
}

HomeController.$inject = ['$scope', 'TileService'];

export default HomeController;
