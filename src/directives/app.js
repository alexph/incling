let app = () => {
  return {
    template: require('../templates/app.html'),
    controller: 'AppController',
    controllerAs: 'app',
    as: 'app',
    replace: true
  }
};

export default app;
