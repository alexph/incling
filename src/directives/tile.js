let tile = () => {
  return {
    template: require('../templates/tile.html'),
    controller: 'TileController',
    controllerAs: 'tileCtrl',
    as: 'tile',
    replace: true
  }
};

export default tile;
