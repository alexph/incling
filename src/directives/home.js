let home = () => {
  return {
    template: require('../templates/home.html'),
    controller: 'HomeController',
    controllerAs: 'homeCtrl',
    as: 'home',
    replace: true
  }
};

export default home;
