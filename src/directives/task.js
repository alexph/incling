let task = () => {
  return {
    template: require('../templates/task.html'),
    controller: 'TaskController',
    controllerAs: 'ctrl',
    as: 'task',
    replace: true
  }
};

export default task;
