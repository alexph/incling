let carouselItem = () => {
  return {
    require: '^^carousel',
    template: require('../templates/carousel-item.html'),
    as: 'carouselSlide',
    replace: true,
    transclude: true,
    link(scope, el, attrs, carouselCtrl) {
      carouselCtrl.registerItem(scope);
    }
  }
};

export default carouselItem;
