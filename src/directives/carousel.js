import debounce from 'lodash/debounce'
import once from 'lodash/once'

let carousel = () => {
    return {
      template: require('../templates/carousel.html'),
      controller: 'CarouselController',
      controllerAs: 'carouselCtrl',
      as: 'carousel',
      replace: true,
      transclude: true,
      link: (scope, el, attrs, carouselCtrl) => {
        const $el = $(el);

        const options = {
          interval: false
        };

        const loadOnce = once(() => {
          $el.find('.carousel-item').first().addClass('active');
          $el.carousel();
        });

        const debounceLoad = debounce(loadOnce, 500);

        scope.$watch(() => {
          return carouselCtrl.carouselItems
        }, (newValue, oldValue) => {
          debounceLoad();
        });

        scope.carouselPrev = (e) => {
          e.preventDefault();
          $el.carousel('prev');
        };

        scope.carouselNext = (e) => {
          e.preventDefault();
          $el.carousel('next');
        };
      }
    }
  }
;

export default carousel;
