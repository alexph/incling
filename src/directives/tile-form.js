let tileForm = () => {
  return {
    template: require('../templates/tile-form.html'),
    controller: 'FormController',
    controllerAs: 'formCtrl',
    as: 'tileForm',
    replace: true
  }
};

export default tileForm;
