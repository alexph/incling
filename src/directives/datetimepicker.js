let datetimepicker = () => {
  return {
    restrict: 'A',
    link(scope, el, attrs) {
      console.log('dtp');
      const id = 'dtp-' + scope.$id;
      el.attr('id', id);
      el.attr('data-target', '#' + id);
      el.attr('data-toggle' , 'datetimepicker');
      $(el).datetimepicker();
    }
  }
};

datetimepicker.$inject = [];

export default datetimepicker;
