let mainNav = () => {
  return {
    template: require('../templates/main-nav.html'),
    as: 'mainNav',
    replace: true
  }
};

export default mainNav;
