
function TileResource($resource, TaskResource) {
  let tileResponse = (response) => {
    return response.resource.map(tile => {
      tile.tasks = tile.tasks.map(task => {
        return new TaskResource(task);
      });
      return tile;
    });
  };

  let interceptor = {
    response: tileResponse
  };

  return $resource('/api/1/tiles/:id/', {id: '@id'}, {
    update: {
      method: 'PUT',
      interceptor
    },
    save: {
      method: 'POST',
      interceptor
    },
    get: {
      method: 'GET',
      interceptor
    },
    query: {
      method: 'GET',
      isArray: true,
      interceptor
    }
  })
}

TileResource.$inject = ['$resource', 'TaskResource'];

export default TileResource;
