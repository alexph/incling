function TileService(TileResource) {
  return {
    getTiles() {
      return TileResource.query().$promise;
    }
  }
}

TileService.$inject = ['TileResource'];

export default TileService;
