function TaskResource($resource) {
  return $resource('/api/1/tasks/:id/');
}

TaskResource.$inject = ['$resource'];

export default TaskResource;
