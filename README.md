# Django and Angular test

## Setup

### Prereq's

Project uses `python3`, `angular@1.15.10`, `webpack 4.5`, `django 2` and `djangorestframework`

```
yarn install --production=false`
pip install -r requirements.txt
```


### Building assets

For development yarn will watch and reload assets:

```
yarn dev
```

For production yarn will build and package assets and generate HTML for the hashed assets.

```
yarn build
```

### Production example

There's a production build example running on Heroku:

`https://alexincling.herokuapp.com/`

Admin is at `/admin`.

Username: `admin`
Password: `letmein`

The example uses `whitenoise` in place of a CDN for demo purposes.
