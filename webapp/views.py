from django.conf import settings
from django.views.generic import TemplateView


class HomeView(TemplateView):
    template_name = 'webapp/home.html'

    def get_template_names(self):
        if settings.DEBUG:
            return [self.template_name]
        return 'webapp/home-production.html'
