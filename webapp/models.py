from django.db import models
from django.db.models.manager import Manager


class Tile(models.Model):
    STATUS_LIVE = 0
    STATUS_PENDING = 1
    STATUS_ARCHIVED = 2

    STATUS_OPTIONS = [
        (STATUS_LIVE, 'Live'),
        (STATUS_PENDING, 'Pending'),
        (STATUS_ARCHIVED, 'Archived')
    ]

    launch_date = models.DateTimeField()
    status = models.PositiveIntegerField(choices=STATUS_OPTIONS, default=STATUS_LIVE)

    objects = Manager()

    class Meta:
        ordering = ('launch_date',)


class Task(models.Model):
    TYPE_EXAMPLE_0 = 0
    TYPE_EXAMPLE_1 = 1

    TYPE_OPTIONS = [
        (TYPE_EXAMPLE_0, 'Example 0'),
        (TYPE_EXAMPLE_1, 'Example 1')
    ]

    tile = models.ForeignKey(Tile, models.CASCADE, related_name='tasks')
    title = models.CharField(max_length=255)
    description = models.TextField(default='', blank=True)
    order = models.PositiveIntegerField()

    objects = Manager()

    class Meta:
        ordering = ('order',)
