from rest_framework.viewsets import ModelViewSet
from .serializers import TileSerializer
from .models import Tile


class TileViewSet(ModelViewSet):
    serializer_class = TileSerializer
    queryset = Tile.objects.all()
