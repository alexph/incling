from django.contrib import admin
from .models import Tile, Task


class TaskInline(admin.TabularInline):
    model = Task
    extra = 0


class TileAdmin(admin.ModelAdmin):
    inlines = (TaskInline,)


admin.site.register(Tile, TileAdmin)
